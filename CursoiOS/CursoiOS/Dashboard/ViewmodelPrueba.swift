//
//  ViewmodelPrueba.swift
//  CursoiOS
//
//  Created by roreyesl on 02/12/21.
//

import Foundation
import UIKit
import CoreData

class ViewmodelPrueba : Decodable {
    
    typealias CompletionBlock = (NSError) -> Void
    private var serviceManager : Webservice?
    
    init(_ serviceManager : Webservice = Webservice()){
        self.serviceManager = serviceManager
    }
    
    var people: [NSManagedObject] = []

    // MARK: - LifeCycle
    required init(from decoder: Decoder) throws {}
    
    // MARK: - Functions

}

//Core Data.
extension ViewmodelPrueba {
    func saveData(name: String) {
      
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      // 1
      let managedContext = appDelegate.persistentContainer.viewContext
      // 2
      let entity = NSEntityDescription.entity(forEntityName: "Persons", in: managedContext)!
      let person = NSManagedObject(entity: entity, insertInto: managedContext)
      // 3
      person.setValue(name, forKeyPath: "name")
      // 4
      do {
        try managedContext.save()
        people.append(person)
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }

    func getData() {
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Persons")
        //3
        do {
            people = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
