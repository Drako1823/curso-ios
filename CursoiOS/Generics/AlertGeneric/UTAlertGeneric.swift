//
//  AlertGeneric.swift
//
//  Created by roreyesl on 13/11/21.
//

import Foundation
import UIKit

class UTAlertGeneric {
    
    //MARK: - Functions
    
    static func simpleWith(title        : String? = "Curso iOS",
                                  message      : String?,
                                  actionTitle  : String = "Aceptar",
                                  actionHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title         : title,
                                                message       : message ?? "",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title  : actionTitle,
                                                style  : .default,
                                                handler: actionHandler))
        return alertController
    }
}
