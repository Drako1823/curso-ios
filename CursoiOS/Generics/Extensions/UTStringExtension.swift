//
//  UTStringExtension.swift
//
//  Created by roreyesl on 13/11/21.
//

import Foundation

extension String {
    var isLetter: Bool { return range(of : "[^a-zA-Z]", options: .regularExpression) == nil && self != "" }
    var isReturn: Bool { return self == "" }
    var isSpace: Bool { return self == " " }
}
