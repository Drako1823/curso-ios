//
//  IntExtension.swift
//
//  Created by roreyesl on 13/11/21.
//

import Foundation

extension Int {
    //MARK: - Properties
    var isSuccess: Bool { return self == 200 }
}
