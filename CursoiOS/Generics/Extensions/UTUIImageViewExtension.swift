//
//  UTUIImageViewExtension.swift
//
//  Created by roreyesl on 13/11/21.
//

import Foundation
import  UIKit

extension UIImageView {
    
    func loadImage(withName strImage: String, withPlaceHolder bPlaceHolder:Bool = true) {
        
        if bPlaceHolder {
            self.image = UIImage(named: "imgPlaceHolder")
        }
        if let url = URL(string: strImage){
            UIImageLoader.loader.load(url, for: self)
        }
    }
    
    func cancelImageLoad() {
        UIImageLoader.loader.cancel(for: self)
    }    
}
