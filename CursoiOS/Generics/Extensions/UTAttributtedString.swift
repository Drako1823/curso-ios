//
//  UTAttributtedString.swift
//
//  Created by roreyesl on 13/11/21.
//

import Foundation
import UIKit

extension NSMutableAttributedString{
    class func setAttributedText(withString string: String, boldString: String, font: UIFont = UIFont.systemFont(ofSize: CGFloat(18.0))) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
}
